package ru.itpark.domain;

public class House implements Comparable<House> {
    private long id;
    private int price;
    private String district;

    public House(long id, int price, String district) {
        this.id = id;
        this.price = price;
        this.district = district;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    @Override
    public int compareTo(House o) {
            return - (price - o.getPrice());
    }
}


package ru.itpark.exception;

public class NegativePriceException extends RuntimeException{
    public NegativePriceException() {
    }

    public NegativePriceException (String message) {super(message);}

    public NegativePriceException (String message, Throwable cause) {super(message, cause);}

    public NegativePriceException (Throwable cause) {super (cause);}

    public NegativePriceException (String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

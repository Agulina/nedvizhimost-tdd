package ru.itpark.service;

import ru.itpark.domain.House;
import ru.itpark.exception.NegativePriceException;

import java.util.ArrayList;

public class Catalog {
    private ArrayList<House> houses = new ArrayList<>();

    public int size() {return houses.size();}

    public void add(House house) {
        houses.add(house);
    }

    public ArrayList<House> findByPrice(int price) {
        ArrayList<House> result = new ArrayList<House>();
        for (House house : houses) {
            if (house.getPrice() <= price) {
                result.add(house);
            }

            if (price < 0) {
                throw new NegativePriceException();
            }
        }
        return result;
    }

    public ArrayList<House> findByDistricts(String... args) {
        ArrayList<House> result = new ArrayList<House>();
        for (String district : args) {
            for (House house : houses) {
                if (district == house.getDistrict() ) {
                    result.add(house);
                }
            }
        }
        return result;
    }
}

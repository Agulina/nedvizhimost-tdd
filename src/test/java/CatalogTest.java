import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.itpark.domain.House;

import ru.itpark.exception.NegativePriceException;
import ru.itpark.service.Catalog;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertNotNull;


public class CatalogTest {
    private Catalog catalog;

    @BeforeEach
    void setUp() {
        catalog = new Catalog();
        catalog.add(new House(1, 1_000_000, "district1"));
        catalog.add(new House(2, 2_000_000, "district2"));
        catalog.add(new House(3, 3_000_000, "district3"));

    }


    @Test
    public void testCreation() {assertNotNull(catalog);};


    @Test
    public void testAddOneToCatalog() {
        int size = catalog.size();
        Assertions.assertEquals(3, size);


    }

    @Test
    public void resultForNonExistencePrice() {
        ArrayList<House> houses = catalog.findByPrice(500_000);
        Assertions.assertEquals(0, houses.size());
    }


    @Test
    public void oneResultForExistentPrice() {
        ArrayList<House> houses = catalog.findByPrice(1_500_000);
        Assertions.assertEquals(1, houses.size());
    }


    @Test
    public void multipleResultForExistentPrice() {
        ArrayList<House> houses = catalog.findByPrice(2_500_000);
        Assertions.assertEquals(2, houses.size());
    }


    @Test
    public void negativePriceException() {
        Assertions.assertThrows(
                NegativePriceException.class,
                () -> {
                    catalog.findByPrice (-1000000);
                }
        );
    }


    @Test
    public void resulForNonExistenceDistrict() {
        ArrayList<House> houses = catalog.findByDistricts("district4");
        Assertions.assertEquals(0, houses.size());
    }

    @Test
    public void oneResultForExistanteDistrict() {
        ArrayList<House> houses = catalog.findByDistricts("district2");
        Assertions.assertEquals(1, houses.size());
    }

    @Test
    public void multipleResultForExistanteDistricts() {
        ArrayList<House> houses = catalog.findByDistricts("district1", "district2");
        Assertions.assertEquals(2, houses.size());
    }


}

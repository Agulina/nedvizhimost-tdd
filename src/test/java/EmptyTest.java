import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ru.itpark.domain.House;
import ru.itpark.exception.NegativePriceException;
import ru.itpark.service.Catalog;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class EmptyTest {
    private Catalog catalog;

    @BeforeEach
    void setUp() {
        catalog = new Catalog();
    }

    @Test
    public void testCreation() {assertNotNull(catalog);}

    @Test
    public void testCatalogSize() {
        int size = catalog.size();
        assertEquals(0, size);
    }

    @Test
    public void testHouseAdding() {
        House house = new House(2, 2_000_000, "district2");
        catalog.add(house);
        int size = catalog.size();
        assertEquals(1, size);
    }

    @Test
    public void testFindByDistrict() {
        String district = "district3";
        List<House> houses = catalog.findByDistricts(district);

        assertEquals(0, houses.size());
    }

    @Test
    public void resultForPrice() {
        ArrayList<House> houses = catalog.findByPrice(1_500_000);
        Assertions.assertEquals(0, houses.size());
    }

    @Test
    public void negativePriceException() {
        Assertions.assertThrows(
                NegativePriceException.class,
                () -> {
                    catalog.findByPrice (-1000000);
                }
        );
    }
}
